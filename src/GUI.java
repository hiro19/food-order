import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI {
    public static void main(String[] args) {
        JFrame frame = new JFrame("GUI");
        frame.setContentPane(new GUI().root);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private JPanel root;
    private JLabel tapLabel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane receivedInfo;
    private JButton yakisobaButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton checkOutButton;
    private JLabel receivedInfo3;

    int total = 0;

    public void order (String food) {
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            receivedInfo.setText(receivedInfo.getText()+ food + "\r\n");
            total = total + 100;
            receivedInfo3.setText( total +" yen");
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food
                    + "! It will be received as soon as possible.");
        }
    }

    void pay (String money){
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to checkout?",
                "Checkout Confirmation",
                JOptionPane. YES_NO_OPTION);
        if(confirmation == 0) {
            receivedInfo.setText("");
            total = 0;
            receivedInfo3.setText(total + " yen" );
            JOptionPane.showMessageDialog(null, "Thank you. The total price is " + money + " yen.");
        }
    }


    public GUI() {


        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");

            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");

            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pay("money");
            }
        });
    }
}
